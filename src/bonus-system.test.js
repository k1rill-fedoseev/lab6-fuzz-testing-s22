import { calculateBonuses } from "./bonus-system.js"

describe('Bonus system', () => {
    const prec = 5

    test('Standard bonus program', () => {
        const program = 'Standard'
        expect(calculateBonuses(program, 5000)).toBeCloseTo(0.05, prec)
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.075, prec)
        expect(calculateBonuses(program, 25000)).toBeCloseTo(0.075, prec)
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.1, prec)
        expect(calculateBonuses(program, 75000)).toBeCloseTo(0.1, prec)
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.125, prec)
        expect(calculateBonuses(program, 150000)).toBeCloseTo(0.125, prec)
    })

    test('Premium bonus program', () => {
        const program = 'Premium'
        expect(calculateBonuses(program, 5000)).toBeCloseTo(0.1, prec)
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.15, prec)
        expect(calculateBonuses(program, 25000)).toBeCloseTo(0.15, prec)
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.2, prec)
        expect(calculateBonuses(program, 75000)).toBeCloseTo(0.2, prec)
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.25, prec)
        expect(calculateBonuses(program, 150000)).toBeCloseTo(0.25, prec)
    })

    test('Diamond bonus program', () => {
        const program = 'Diamond'
        expect(calculateBonuses(program, 5000)).toBeCloseTo(0.2, prec)
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.3, prec)
        expect(calculateBonuses(program, 25000)).toBeCloseTo(0.3, prec)
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.4, prec)
        expect(calculateBonuses(program, 75000)).toBeCloseTo(0.4, prec)
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.5, prec)
        expect(calculateBonuses(program, 150000)).toBeCloseTo(0.5, prec)
    })

    test('Invalid program test', () => {
        const program = 'xyz'
        expect(calculateBonuses(program, 5000)).toBeCloseTo(0, prec)
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0, prec)
        expect(calculateBonuses(program, 25000)).toBeCloseTo(0, prec)
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0, prec)
        expect(calculateBonuses(program, 75000)).toBeCloseTo(0, prec)
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0, prec)
        expect(calculateBonuses(program, 150000)).toBeCloseTo(0, prec)
    })
})
